//
//  AppDelegateTests.m
//  NIXProject
//
//  Created by Nezhelskoy Iliya on 11/9/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AppDelegate.h"

@interface AppDelegateTests : XCTestCase

@end

@implementation AppDelegateTests

- (void)testWindowProperty
{
    AppDelegate *appDelegate = [AppDelegate new];
    
    UIWindow *window = [UIWindow new];
    
    // exercise
    [appDelegate setWindow:window];
    
    // verify
    XCTAssertEqualObjects(window, [appDelegate window]);
}

@end
