Legend:

[Changes] - What changes were in the project.
[TODO]    - Сhanges which should be made in the project.
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 101
author: Nezhelskoy Iliya
date  : 11-oct-2016
  
[Changes] 1. Updated AFNetworking [~> 3.1.0]
[Changes] 2. Updated OCMock [~> 3.3.1]

[TODO] 1. Podfile: Set version of AFNetworking to `~> 3.1.0`
       2. Podfile: Set version of OCMock to `~> 3.3.1`
       3. Re-intall pods
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 100
author: Nezhelskoy Iliya
date  : 11-oct-2016
  
[Changes] 1. Upgraded podfile according to the latest cocoapods DSL syntax [1.0.1]
[Changes] 2. Re-installed pods

[TODO] 1. Update cocoapods gem
       2. Re-install pods
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 99
author: Nezhelskoy Iliya
date  : 25-jul-2016
  
[Changes] 1. Increased iOS deployment target of project [-> 9.0]
[Changes] 2. Re-installed pods according to updated deployment target
[Changes] 3. Fix header of XCTestAppDelegate

[TODO] 1. In project settings set deployment target of project to 9.0
       2. Reset base configuration of 'Application' scheme
       3. Re-install pods
       4. In header of "XCTestAppDelegate.m" set file extension to ".m"
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 98
author: Nezhelskoy Iliya
date  : 24-jul-2016
  
[Changes] 1. Increased iOS deployment target in Podfile [-> 9.0]
[TODO] 1. In "Podfile" set "platform :ios" to '9.0'
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 96
author: Nezhelskoy Aleksey
date  : 13-jan-2016
  
[Changes] 1. Reset code signing resources path to “” ( to prevent upload error during uploading build to Appstore)
[Changes] 2. Disable bitcode for project and pods.
[Changes] 3. Disable new multitasking mode for iPad app by default ( to prevent upload error during uploading build to Appstore)
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 95
author: Ruslan Maley
date  : 19-oct-2015
  
[Changes] 1. Changed ObjCLint version

[TODO]    1. Update NIXObjCLint script submodule to new version
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 88
author: Iliya Nezhelskoy
date  : 03-oct-2014
	
[Changes] 1. Added "$(inherited)" flag to "HEADER_SEARCH_PATHS".

[TODO] 	  1. Open Build Settings.
          2. Add $(inherited) flag to HEADER_SEARCH_PATHS.
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 81
author: Egor Zubkov
date  : 16-may-2014
	
[Changes] 1. Added "NIXObjCLint" build phase to the "Tests" target.

[TODO] 	  1. Create "NIXObjCLint.py" build phase in the "Tests" target.
          2. Add executable script to added build phase:
             ./Scripts/NIXObjCLint/NIXObjCLint.py --source="${SRCROOT}/Source"
————————————————————————————————————————————————————————————————————————————————————————————————————
tag   : 72
author: Sergey Nazarenko
date  : 17-mar-2014
	
[Changes] 1. Assets are supported.
		  2. Script |AddBuildNumberOnIcon.sh| works faster.

[TODO] 	  1. Update |NIXBuildAutomationScripts| for tag 65.
          2. Remove icons from resources.
          3. Add asset in resources.
          4. Select added asset in general settings.
          5. In "Info.plist" file remove all keys which contain icons(CFBundleIconFiles).
          6. In build phase |AddBuildNumberOnIcon.sh| set path for asset. For example "Resources/Icons" change on "Resources/Images/Images.xcassets".
————————————————————————————————————————————————————————————————————————————————————————————————————